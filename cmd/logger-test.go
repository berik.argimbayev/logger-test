package main

import (
	"fmt"
	"gitlab.com/berik.argimbayev/logger-test/pkg/house"
	"go.uber.org/zap"
	"log"
)

func main() {
	logger, err := zap.NewProduction()
	logger = logger.With(zap.String("Extra", "yes"))
	if err != nil {
		log.Fatal(err)
	}
	logger.Info("add",zap.String("address", fmt.Sprintf("%v",&logger)))
	h, err := house.NewHouse(true, house.WithLogger(*logger))
	fmt.Println(h)
}
