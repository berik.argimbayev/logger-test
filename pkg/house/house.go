package house

import (
	"fmt"
	"go.uber.org/zap"
)

type House struct {
	walls   bool
	cookies int
	logger  *zap.Logger
}

func NewHouse(walls bool, options ...func(*House)) (*House, error) {
	h := &House{walls: walls}

	h.cookies = 0
	var err error
	h.logger, err = zap.NewProduction()
	if err != nil {
		return nil, err
	}
	for _, o := range options {
		o(h)
	}
	h.logger.Info("check")
	return h, nil
}

func WithLogger(logger zap.Logger) func(*House) {
	return func(h *House) {
		h.logger = &logger
		h.logger.Info("add",zap.String("address", fmt.Sprintf("%v",&logger)))
		/*
		h.logger = logger.With(
			zap.String("Field", "grass"),
			zap.String("Extra", "no"),
		)*/
	}
}